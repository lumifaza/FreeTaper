#include <iostream>
#include <random>
#include <ctime>
#include <array>
#include <string>
#include <type_traits>
#include <regex>

#include "util.hpp"
#include "strutils.hpp"
#include "app_version.hpp"

#include "RingBuffer.hpp"
#include "ProcessExecArgs.hpp"
#include "BufferToProcesses.hpp"
#include "capture.hpp"
#include "number_wrappers.hpp"
#include "SampleFormat.hpp"
#include "SampleConverter.hpp"

#include "hjson_sugar.hpp"
#include "globals.hpp"
#include "specs.hpp"

struct RingBufferDescriptor {
    RingBuffer<uint8_t> rb;
    size_t stride;
    RingBufferDescriptor(size_t size, size_t a_stride): rb(size), stride(a_stride) {
        std::cerr << "Created RingBufferDescriptor with RB bytes = " << size << ", stride bytes = " << a_stride << std::endl;
    };
};


static std::tm timeToTm(const time_t ts) {
    std::tm tms;
    if (global_use_local_time) {
        localtime_r(&ts, &tms);
    } else {
        gmtime_r(&ts, &tms);
    }
    return tms;
}

class FilesWriter {
protected:
    boost::asio::io_service &io_;
    std::unique_ptr<BufferToProcesses> btp_;
    RingBuffer<uint8_t> &ringbuffer_;
    size_t stride_;
    using GetArgsFunction = std::function<ProcessExecArgs(const std::tm&)>;
    std::vector<GetArgsFunction> get_args_fns_;
    std::tm new_part_start_tm_;
    bool has_new_part_start_tm_ = false;
    size_t bytes_before_current_part_ = 0;
    bool closing_ = false;
    static std::string getOutputFileName(StreamSpec &spec, StreamHandlerSpec &hspec, const std::tm &time) {
        using namespace boost::filesystem;
        char buff[2048];
        std::strftime(buff, 2047, hspec.arg.c_str(), &time);
        path file_path(buff);
        path dir_path = file_path.parent_path();
        if (!dir_path.empty()) {
            create_directories(dir_path);
        }
        return {buff};
    };
    bool processWorks(const bool resurrect = false) {
        bool works = btp_ && !btp_->closed();
        if (works) {
            return true;
        } else {
            if (resurrect) {
                if (!btp_ || btp_->readyToDestroy()) { // btp_ not created yet or already exited
                    if (btp_) {
                        bytes_before_current_part_ += btp_->totalBytes();
                    }
                    if (!has_new_part_start_tm_) {
                        std::cerr << "Some of outputs died unexpectedly (encoder bug? OOM killer? admin?). Starting new part." << std::endl;
                        new_part_start_tm_ = timeToTm(time(nullptr));
                    }
                    btp_ = make_unique<BufferToProcesses>(io_, ringbuffer_, get_args_fns_.size());
                    for (auto &fn: get_args_fns_) {
                        btp_->addProcess(fn(new_part_start_tm_));
                    }
                    has_new_part_start_tm_ = false;
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }
    /*void setPartStartToNow() {
        time_t t = time(nullptr);
        gmtime_r(&t, &new_part_start_tm_);
    }*/
public:
    FilesWriter(boost::asio::io_service &io, RingBufferDescriptor &ringbuffer_desc, StreamSpec &spec, int sample_rate, int estimated_length):
        io_(io), ringbuffer_(ringbuffer_desc.rb), stride_(ringbuffer_desc.stride) {
        if (spec.format.isFloat()) {
            throw std::runtime_error("Floating point output is not supported"); // TODO
        }
        get_args_fns_.reserve(spec.handlers.size());
        for (StreamHandlerSpec &hspec: spec.handlers) {
            strutils::toLowerInPlace(hspec.name);
            
            if (hspec.name==std::string("flac")) {
                size_t seekpoint_every = 20;
                size_t seekpoints = (estimated_length>0) ? (estimated_length/seekpoint_every) : 0;
                seekpoint_every *= sample_rate;
                get_args_fns_.push_back([&spec, &hspec, sample_rate, seekpoint_every, seekpoints](const std::tm &time) -> ProcessExecArgs {
                    ProcessExecArgs pea("/usr/bin/flac");
                    pea.args.reserve(10 + seekpoints);
                    pea.args.insert(pea.args.end(), {
                        "--padding=4096",
                        "--force-raw-format",
                        std::string("--endian=") + (spec.format.realEndianness()==SampleFormat::ENDIAN_LITTLE ? "little" : "big"),
                        "--bps=" + std::to_string(spec.format.bits()),
                        "--sample-rate=" + std::to_string(sample_rate),
                        "--sign=signed",
                        "--channels=" + std::to_string(spec.channels.size()),
                        "-o",
                        getOutputFileName(spec, hspec, time)
                    });
                    size_t seekpoint = 0;
                    for (size_t i=0; i<seekpoints; i++) {
                        pea.args.push_back(std::string("-S") + std::to_string(seekpoint));
                        seekpoint += seekpoint_every;
                    }
                    pea.args.emplace_back("-");
                    return pea;
                });
            } else if (hspec.name==std::string("vorbis")) {
                get_args_fns_.push_back([&spec, &hspec, sample_rate](const std::tm &time) -> ProcessExecArgs {
                    return {
                        "/usr/bin/oggenc",
                        "--raw",
                        "--raw-bits=" + std::to_string(spec.format.bits()),
                        "--raw-chan=" + std::to_string(spec.channels.size()),
                        "--raw-rate=" + std::to_string(sample_rate),
                        std::string("--raw-endianness=") + (spec.format.realEndianness()==SampleFormat::ENDIAN_LITTLE ? "0" : "1"),
                        "--skeleton",
                        "--quiet",
                        "-q",
                        "8", // TODO: quality modification in config
                        "-o",
                        getOutputFileName(spec, hspec, time),
                        "-"
                    };
                });
            } else if (hspec.name==std::string("wav")) {
                if (spec.format.realEndianness() != SampleFormat::ENDIAN_LITTLE) {
                    throw std::runtime_error("wav format requires little endian samples");
                }
                get_args_fns_.push_back([&spec, &hspec, sample_rate](const std::tm &time) -> ProcessExecArgs {
                    return {
                        "./freetaper-writewav", // TODO: detect path, maybe configurable?
                        getOutputFileName(spec, hspec, time),
                        std::to_string(sample_rate),
                        std::to_string(spec.format.bits()),
                        std::to_string(spec.channels.size())
                    };
                });
            } else {
                throw std::runtime_error("Unsupported output handler: " + hspec.name);
            }
        }
    }
    void setPartStartTime(const std::tm &start_tm) {
        new_part_start_tm_ = start_tm;
        has_new_part_start_tm_ = true;
    }
    void pollBuffer() {
        if (processWorks(!closing_)) btp_->pollBuffer();
    }
    void finish() {
        closing_ = true;
        if (!processWorks()) return;
        btp_->finish();
    }
    bool closed() {
        if (!closing_) return false; // closed btp_ is not considered closed *this if closing wasn't requested
        if (!btp_) return true;
        return btp_->closed();
    }
    void splitWhenTotalFramesReach(size_t frames) {
        if (!processWorks()) return;
        if (btp_->isFinishScheduled()) return;
        btp_->finishWhenTotalBytesReach(frames*stride_ - bytes_before_current_part_);
    }
};

using TotalFramesGetFunction = std::function<size_t()>;

class WritingGroup {
protected:
    std::vector<FilesWriter> writers_;
    boost::asio::io_service &io_ = global_io_service;
    boost::asio::deadline_timer poll_timer_;
    bool do_work_ = false;
    std::thread thread_;
    TotalFramesGetFunction total_frames_get_fn_;
    int split_every_ = 0;
    time_t prev_time_div_ = 0;
    void setNextPartTime(const time_t ts) {
        std::tm tms = timeToTm(ts);
        for (FilesWriter &writer: writers_) {
            writer.setPartStartTime(tms);
        }
    }
    void poll(const boost::system::error_code &ec) {
        //std::cerr << "poll begin\n";
        if (do_work_ && split_every_>0) {
            time_t curr_time = time(nullptr);
            time_t curr_time_div = curr_time/split_every_;
            if (curr_time_div != prev_time_div_ && prev_time_div_ != 0) {
                size_t total_frames = total_frames_get_fn_();
                setNextPartTime(curr_time);
                for (FilesWriter &writer: writers_) {
                    writer.splitWhenTotalFramesReach(total_frames);
                }
            }
            prev_time_div_ = curr_time_div;
        }
        bool all_closed = true;
        for (FilesWriter &writer: writers_) {
            if (!writer.closed()) {
                writer.pollBuffer();
                all_closed = false;
            }
        }
        if (do_work_ || !all_closed) {
            //std::cerr << "Activating poll timer\n";
            activatePollTimer();
        }
        //std::cerr << "poll end\n";
    }
    void activatePollTimer() {
        poll_timer_.expires_from_now(boost::posix_time::milliseconds(100));
        poll_timer_.async_wait([this](const boost::system::error_code &ec) {
            poll(ec);
        });
    }
    void worker() {
        set_thread_name("IO");
        io_.run();
    }
public:
    WritingGroup(): poll_timer_(io_) {
    }
    void enableSplitting(const int split_every, TotalFramesGetFunction fn) {
        total_frames_get_fn_ = fn;
        split_every_ = split_every;
    }
    void reserveWriters(const size_t count) {
        writers_.reserve(count);
    }
    void addWriter(RingBufferDescriptor &rbd, StreamSpec &spec) {
        writers_.emplace_back(io_, rbd, spec, global_sample_rate, split_every_);
    }
    void start() {
        if (do_work_) return;
        do_work_ = true;
        setNextPartTime(time(nullptr));
        activatePollTimer();
        thread_ = std::thread([this]() { worker(); });
        setThreadRealTime(thread_, global_prio_writer);
    }
    void stop() {
        if (!do_work_) return;
        do_work_ = false;
        for (FilesWriter &writer: writers_) {
            writer.finish();
        }
        thread_.join();
    }
    decltype(io_)& io() { return io_; }
};


struct ConversionDescriptor;
class ConversionGroup;

struct ConversionDescriptor {
    RingBufferDescriptor &src, &dst;
    size_t src_offset, dst_offset, position;
    ConversionFunction* convert;
    ConversionDescriptor(RingBufferDescriptor &a_src, RingBufferDescriptor &a_dst, const size_t a_src_offset, const size_t a_dst_offset, ConversionFunction* a_convert):
        src(a_src), dst(a_dst), src_offset(a_src_offset), dst_offset(a_dst_offset), position(0), convert(a_convert) {
        std::cerr << "Created ConverionDescriptor with src_offset " << src_offset << ", dst_offset " << dst_offset << std::endl;
    }
};

class ConversionGroup {
protected:
    SampleConverter conv_;
    std::vector<ConversionDescriptor> descriptors_;
    std::vector<RingBufferDescriptor> sources_, destinations_;
    size_t position_to_subtract_ = 0;
    size_t source_rb_length_;
    size_t destination_rb_length_;
    bool do_work_ = false;
    std::thread thread_;
    
    void convertByDescriptor(ConversionDescriptor &descr) {
        // descr.position is to be decremented outside of this function
        // together with advancing src.rb and dst.rb
        auto src_buff = descr.src.rb.getReadBuffer();
        auto dst_buff = descr.dst.rb.getWriteBuffer();
        size_t src_count = src_buff.count() / descr.src.stride;
        size_t dst_count = dst_buff.count() / descr.dst.stride;
        const size_t count = std::min(src_count, dst_count);
        uint8_t* src_ptr = src_buff.begin() + descr.src_offset;
        uint8_t* dst_ptr = dst_buff.begin() + descr.dst_offset;
        descr.convert(conv_.getFirstArgument(),
            src_ptr, descr.src.stride,
            dst_ptr, descr.dst.stride,
            descr.position, count);
    }
    bool convertAll() {
        size_t min_pos = SIZE_MAX;
        for (ConversionDescriptor &descr: descriptors_) {
            assert(descr.position >= position_to_subtract_);
            descr.position -= position_to_subtract_;
            convertByDescriptor(descr);
            if (descr.position < min_pos) min_pos = descr.position;
        }
        
        position_to_subtract_ = min_pos;
        if (min_pos==0) return false;
        
        for (RingBufferDescriptor &rbd: sources_) {
            rbd.rb.readDone(min_pos * rbd.stride);
        }
        for (RingBufferDescriptor &rbd: destinations_) {
            rbd.rb.writeDone(min_pos * rbd.stride);
        }
        
        return true;
    }
public:
    decltype(sources_)& sourceRingBuffers() { return sources_; };
    decltype(destinations_)& destinationRingBuffers() { return destinations_; };
    template<typename Container> static void findChannel(const Container &streams, const ChannelNumber req_ch, int &stream_index, int &ch_index) {
        for (int i=0; i<streams.size(); i++) {
            auto &stream = streams[i];
            for (int j=0; j<stream.channels.size(); j++) {
                if (stream.channels[j]==req_ch) {
                    stream_index = i;
                    ch_index = j;
                    return;
                }
            }
        }
        stream_index = -1;
        ch_index = -1;
    }
    
    template<typename InContainer, typename OutContainer> void createDescriptors(InContainer &in_specs, OutContainer &out_specs) {
        assert(sources_.size()==0 && descriptors_.size()==0 && descriptors_.size()==0);
        sources_.reserve(in_specs.size());
        for (StreamSpec &in_spec: in_specs) {
            sources_.emplace_back(in_spec.bytesPerFrame() * source_rb_length_, in_spec.bytesPerFrame());
        }
        destinations_.reserve(out_specs.size());
        
        size_t descriptors_count = 0;
        for (StreamSpec &out_spec: out_specs) {
            descriptors_count += out_spec.channels.size();
        }
        descriptors_.reserve(descriptors_count);
        
        for (StreamSpec &out_spec: out_specs) {
            destinations_.emplace_back(out_spec.bytesPerFrame() * destination_rb_length_, out_spec.bytesPerFrame());
            SampleFormat out_format = out_spec.format;
            for (int out_ch_index=0; out_ch_index<out_spec.channels.size(); out_ch_index++) {
                ChannelNumber flat_ch_id = out_spec.channels[out_ch_index];
                int in_stream_index, in_ch_index;
                findChannel(in_specs, flat_ch_id, in_stream_index, in_ch_index);
                if (in_stream_index<0 || in_ch_index<0) {
                    throw std::runtime_error("Invalid channel number: " + std::to_string(flat_ch_id));
                }
                SampleFormat in_format = in_specs[in_stream_index].format;
                descriptors_.emplace_back(
                    sources_[in_stream_index], // source ringbuffer
                    destinations_.back(), // destination ringbuffer
                    in_ch_index * in_format.bytes(), // source offset
                    out_ch_index * out_format.bytes(), // destination offset
                    conv_.getConversionFunction(in_format, out_format) // function to execute
                );
                if (descriptors_.back().convert==nullptr) {
                    throw std::runtime_error("Invalid format specification");
                }
            }
        }
        assert(descriptors_.size() == descriptors_count);
    }
    template<typename InContainer, typename OutContainer> ConversionGroup(InContainer &in_specs, OutContainer &out_specs, size_t source_rb_length, size_t destination_rb_length): source_rb_length_(source_rb_length), destination_rb_length_(destination_rb_length) {
        createDescriptors(in_specs, out_specs);
    }
    void worker() {
        set_thread_name("Converter");
        while(true) {
            while (convertAll()) {};
            if (!do_work_) {
                bool all_consumed = true;
                for (RingBufferDescriptor &rbd: sources_) {
                    if (rbd.rb.fillCount() >= rbd.stride) { // >=stride, not >0, to ignore orphan bytes
                        all_consumed = false;
                        break;
                    }
                }
                if (all_consumed) break;
            }
            usleep(111 * 1000);
        }
    }
    void start() {
        if (do_work_) return;
        do_work_ = true;
        thread_ = std::thread([this]() { worker(); });
        setThreadRealTime(thread_, global_prio_converter);
    }
    void stop() {
        if (!do_work_) return;
        do_work_ = false;
        thread_.join();
    }
};


struct RecorderConfig {
    const char* default_config = "{ \
        capture: { \
            type: 'audio', \
            engine: '.*', \
            device: '.*', \
            channels: 2, \
            format: 'S32', \
            input_buffer: null, \
        }, \
        sample_rate: null, \
        local_time: false, \
        buffer_seconds: { \
            pre_converter: 3, \
            post_converter: 20, \
        }, \
        priority: { \
            converter: null, \
            writer: null \
        }, \
        split: null, \
        out_prefix: '' \
    }";
    std::regex match_api {".*"};
    std::regex match_device {".*"};
    int split_every = -1;
    int samplerate = -1;
    int in_period = -1;
    size_t buffer_pre = 0;
    size_t buffer_post = 0;
    int prio_converter = INT_MIN;
    int prio_writer = INT_MIN;
    bool local_time = false;
    std::string out_prefix;
    PipeSpec in_spec, out_spec;
    JsonValue capture_config;
    void parse(std::istream &config_stream) {
        std::vector<char> config_buffer;
        size_t buffsize = 65536;
        size_t total_read = 0;
        while (!config_stream.eof()) {
            config_buffer.resize(total_read + buffsize);
            config_stream.read(&config_buffer[total_read], buffsize);
            total_read += config_stream.gcount();
        }
        Hjson::Value default_cfg = Hjson::Unmarshal(default_config);
        Hjson::Value read_cfg = Hjson::Unmarshal(&config_buffer[0], total_read);
        Hjson::Value cfg = Hjson::Merge(default_cfg, read_cfg);
        
        capture_config = cfg["capture"];
        
        samplerate = cfg["sample_rate"];
        if (cfg["split"]) split_every = cfg["split"];
        local_time = (bool)cfg["local_time"];
        
        if (cfg["buffer_samples"]) {
            buffer_pre  = cfg["buffer_samples"]["pre_converter"];
            buffer_post = cfg["buffer_samples"]["post_converter"];
        } else {
            buffer_pre  = (double)cfg["buffer_seconds"]["pre_converter"]  * (double)samplerate;
            buffer_post = (double)cfg["buffer_seconds"]["post_converter"] * (double)samplerate;
        }
        
        if (cfg["priority"]["converter"]) prio_converter = cfg["priority"]["converter"];
        if (cfg["priority"]["writer"]) prio_writer = cfg["priority"]["writer"];
        
        out_prefix = (const char*)cfg["out_prefix"];
        Hjson::forEachInVector(cfg["outputs"], [&](Hjson::Value &jout) {
            out_spec.streams.emplace_back();
            auto &sspec = out_spec.streams.back();
            sspec.format = SampleFormat::fromName(jout["format"]);
            auto addHandler = [&sspec, this](Hjson::Value &jh) {
                sspec.handlers.emplace_back();
                sspec.handlers.back().name = (const char*)jh["type"];
                sspec.handlers.back().arg = out_prefix + (const char*)jh["file_name"];
            };
            if (jout["outputs"]) {
                Hjson::forEachInVector(jout["outputs"], [&](Hjson::Value &jsubout) {
                    addHandler(jsubout);
                });
            } else {
                addHandler(jout);
            }
            auto jchannels = jout["channels"];
            sspec.channels.resize(jchannels.size());
            for (size_t i=0; i<jchannels.size(); i++) {
                sspec.channels[i] = jchannels[i] - 1;
            };
        });
        in_spec = CaptureFactory::getSpec(capture_config);
    }
};


class Recorder {
protected:
    std::unique_ptr<ICapture> capture_;
    ConversionGroup converter_;
    WritingGroup writer_;
    size_t prev_overflows_ = 0;
public:
    Recorder(RecorderConfig &cfg):
        capture_(CaptureFactory::create(std::move(cfg.capture_config))),
        converter_(cfg.in_spec.streams, cfg.out_spec.streams, cfg.buffer_pre, cfg.buffer_post) {
        if (capture_->sampleRate() != global_sample_rate && capture_->sampleRate() >= 0) {
            throw std::runtime_error("Audio device opened itself with sample rate " + std::to_string(capture_->sampleRate()) +
                                     " but " + std::to_string(global_sample_rate) + " requested");
        }
        if (cfg.split_every>0) {
            writer_.enableSplitting(cfg.split_every, [this]() -> size_t {
                return capture_->totalFrames();
            });
        }
        
        writer_.reserveWriters(cfg.out_spec.streams.size());
        assert(cfg.out_spec.streams.size() == converter_.destinationRingBuffers().size());
        for (size_t i=0; i<cfg.out_spec.streams.size(); i++) {
            writer_.addWriter(converter_.destinationRingBuffers()[i], cfg.out_spec.streams[i]);
        }
    }
    void start() {
        capture_->setRingBuffer(&converter_.sourceRingBuffers()[0].rb);
        capture_->start();
        converter_.start();
        writer_.start();
    }
    void stop() {
        // FIXME: if audio_io_ goes mad (e.g. JACK server is killed), it will freeze here
        // the user will then need to SIGKILL
        capture_->stop();
        converter_.stop();
        writer_.stop();
    }
    void printStats(std::ostream &ost) {
        bool warn = false;
        for (RingBufferDescriptor &rbd: converter_.sourceRingBuffers()) {
            if (rbd.rb.fillCount() > rbd.rb.emptyCount()) warn = true;
        }
        for (RingBufferDescriptor &rbd: converter_.destinationRingBuffers()) {
            if (rbd.rb.fillCount() > rbd.rb.emptyCount()) warn = true;
        }
        
        size_t overflows = capture_->overflowCount();
        if (overflows != prev_overflows_) {
            ost << "Input overflow " << overflows-prev_overflows_ << "x!" << std::endl;
            prev_overflows_ = overflows;
            warn = true;
        }
        
        if (!warn) return;
        
        auto printRingBuffer = [&ost](RingBufferDescriptor &rbd) {
            ost << (rbd.rb.fillCount() * 100 / rbd.rb.size()) << "% of " << rbd.rb.size() << " bytes";
        };
        ost << "Before converter: ";
        for (RingBufferDescriptor &rb: converter_.sourceRingBuffers()) {
            printRingBuffer(rb);
        }
        ost << "; After converter: ";
        for (RingBufferDescriptor &rb: converter_.destinationRingBuffers()) {
            printRingBuffer(rb);
            ost << ", ";
        }
        ost << std::endl;
    }
};

bool global_dowork = true;

void exit_handler(int) {
    std::cout << "Exit signal received." << std::endl;
    global_dowork = false;
}


int main(int argc, char** argv) {
    std::cout << APP_VERSION << std::endl;
    
    RecorderConfig config;
    assert(argc > 1);
    {
        std::ifstream configstream(argv[1]);
        config.parse(configstream);
    }
    global_sample_rate = config.samplerate;
    global_prio_converter = config.prio_converter;
    global_prio_writer = config.prio_writer;
    global_use_local_time = config.local_time;
    signal(SIGINT, exit_handler);
    signal(SIGTERM, exit_handler);
    
    Recorder recorder(config);
    //config = {}; // dealloc some RAM
    
    std::cout << "Starting..." << std::endl;
    recorder.start();
    set_thread_name("FreeTaper");
    std::cout << "Started." << std::endl;
    
    while(global_dowork) {
        recorder.printStats(std::cout);
        usleep(1000000);
    }
    /*std::string s;
    std::getline(std::cin, s);*/
    
    std::cout << "Stopping..." << std::endl;
    recorder.stop();
    std::cout << "Stopped." << std::endl;
};
