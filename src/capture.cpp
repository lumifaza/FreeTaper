#include "capture.hpp"

#include "globals.hpp"
#include "RingBuffer.hpp"
#include "util.hpp"
#include "specs.hpp"
#include "SampleFormat.hpp"
#include <iostream>
#include <atomic>

#if ENABLE_PORTAUDIO_CAPTURE
#include "PortAudioIO.hpp"
#endif

#if ENABLE_PROCESS_CAPTURE
#include "ProcessExecArgs.hpp"
#include <boost/process.hpp>
#endif


class BaseCapture: public ICapture {
protected:
    int channels_count_;
    SampleFormat sample_format_;
    RingBuffer<uint8_t> *buffer_;
    uint_fast16_t bytes_per_frame_;
    std::atomic_uint overflow_count_ {0};
    std::atomic_uint total_frames_captured_ {0};
public:
    BaseCapture(const int channels_count, const SampleFormat format):
        channels_count_(channels_count),
        sample_format_(format),
        bytes_per_frame_(channels_count_ * sample_format_.bytes()) {
    }
#if ENABLE_PORTAUDIO_CAPTURE
    PaSampleFormat paSampleFormat() {
        return sample_format_.paSampleFormat();
    }
#endif
    int inputChannelsCount() const { return channels_count_; }
    static int outputChannelsCount() { return 0; }
    virtual unsigned overflowCount() const override { return overflow_count_; }
    virtual size_t totalFrames() const override { return total_frames_captured_.load(std::memory_order_acquire); }
    virtual void setRingBuffer(RingBuffer<uint8_t>* buffer) override {
        buffer_ = buffer;
    }
    void overflow() {
        overflow_count_++;
    }
};

class InterleavedCapture: public BaseCapture {
public:
    using BaseCapture::BaseCapture;
    static PipeSpec outputSpec(int channels_count, const SampleFormat format) {
        // there's single RingBuffer with multiple channels, interleaved
        // so create PipeSpec with single stream having multiple channels
        PipeSpec spec;
        spec.streams.emplace_back();
        spec.streams.back().format = format;
        spec.streams.back().channels.resize(channels_count);
        for (ChannelNumber i=0; i<channels_count; i++) {
            spec.streams.back().channels[i] = i;
        }
        return spec;
    }
};

#if ENABLE_PORTAUDIO_CAPTURE

class BaseCallbackCapture: public InterleavedCapture {
public:
    //using Sample = float;
    using InputBuffer = const uint8_t*;
    using OutputBuffer = uint8_t*;
    using InterleavedCapture::InterleavedCapture;
    void processBuffer(int frame_count, const uint8_t* input, uint8_t* output) {
        (void)output;
        size_t remaining_bytes = frame_count * bytes_per_frame_;
        //const Sample* in_end = input + remaining_samples;
        while (remaining_bytes > 0) {
            auto part = buffer_->getWriteBuffer(remaining_bytes, remaining_bytes);
            if (!part) {
                overflow();
                frame_count -= remaining_bytes / bytes_per_frame_;
                remaining_bytes = 0;
            } else {
                std::copy(input, input + part.count(), part.begin());
                input += part.count();
                remaining_bytes -= part.count();
                buffer_->writeDone(part.count());
            }
        }
        total_frames_captured_.store(total_frames_captured_.load(std::memory_order_relaxed) + frame_count, std::memory_order_release);
    };
};

template<template<typename> class IO> class CallbackCapture: public BaseCallbackCapture {
protected:
    IO<BaseCallbackCapture*> io_;
public:
    template<typename ...Args> CallbackCapture(const int channels_count, const SampleFormat format, Args...args):
        BaseCallbackCapture(channels_count, format),
        io_(this, args...) {
    }
    virtual void start() override {
        set_thread_name("Capture");
        io_.start();
    }
    virtual void stop() override {
        io_.stop();
    }
    virtual int sampleRate() const override {
        return io_.sampleRate();
    }
};

class PortAudioCapture: public CallbackCapture<PortAudioIO> {
public:
    using CallbackCapture<PortAudioIO>::CallbackCapture;
};

#endif // ENABLE_PORTAUDIO_CAPTURE

#if ENABLE_PROCESS_CAPTURE
class ProcessCapture: public InterleavedCapture {
protected:
    using io_service = boost::asio::io_service;
    io_service &io_;
    size_t orphan_bytes_ = 0;
    boost::process::group process_group_;
    boost::process::async_pipe pipe_source_;
    boost::process::child process_;
    boost::asio::deadline_timer retry_timer_;
    std::mutex working_;
public:
    ProcessCapture(const int channels_count, const SampleFormat format, io_service &io, ProcessExecArgs args):
        InterleavedCapture(channels_count, format),
        io_(io),
        pipe_source_(io_),
        retry_timer_(io_) {
        process_ = boost::process::child(args.exe_path, process_group_, boost::process::args(args.args),
            boost::process::std_in < boost::process::null,
            boost::process::std_out > pipe_source_
        );
    }
protected:
    void readBuffer() {
        auto part = buffer_->getWriteBuffer();
        if (part) {
            auto asio_buffer = boost::asio::buffer((void*)part.begin(), part.count());
            boost::asio::async_read(pipe_source_, asio_buffer, [this](const boost::system::error_code &ec, size_t nbytes) {
                if (ec) {
                    if (ec==boost::asio::error::eof) {
                        std::cerr << "End of stream from process." << std::endl;
                    } else {
                        std::cerr << "Error reading from pipe: " << ec.message() << std::endl;
                    }
                    working_.unlock();
                    return;
                }
                buffer_->writeDone(nbytes);
                nbytes += orphan_bytes_;
                total_frames_captured_.store(total_frames_captured_.load(std::memory_order_relaxed) + nbytes / bytes_per_frame_, std::memory_order_release);
                orphan_bytes_ = nbytes % bytes_per_frame_;
                if (pipe_source_.is_open()) {
                    readBuffer();
                } else {
                    std::cerr << "Pipe from process is no longer open." << std::endl;
                    working_.unlock();
                }
            });
        } else {
            // ring buffer full, retry after a while
            retry_timer_.expires_from_now(boost::posix_time::milliseconds(100));
            retry_timer_.async_wait([this](const boost::system::error_code &ec) {
                readBuffer();
            });
        }
    }
public:
    virtual void start() override {
        // please note that this function must be run before starting event loop thread! otherwise race conditions may occur...
        working_.lock();
        readBuffer();
    }
    virtual void stop() override {
        kill(process_.id(), SIGINT);
        working_.lock();
        working_.unlock();
    }
};

#endif // ENABLE_PROCESS_CAPTURE


CaptureFactory::CapturePtr CaptureFactory::create(JsonValue config){
    const std::string type = (const char*)config["type"];
    const int channels_count = config["channels"];
    const SampleFormat format = SampleFormat::fromName(config["format"]);
#if ENABLE_PORTAUDIO_CAPTURE
    if (type=="audio") {
        double suggested_latency = -1;
        if (config["input_buffer"]) {
            suggested_latency = (double)config["input_buffer"]/(double)global_sample_rate;
        }
        return make_unique<PortAudioCapture>(channels_count, format, std::regex(config["engine"]), std::regex(config["device"]), global_sample_rate, suggested_latency);
    }
#endif
#if ENABLE_PROCESS_CAPTURE
    if (type=="process") return make_unique<ProcessCapture>(channels_count, format, global_io_service, ProcessExecArgs::fromList((JsonValue)config["args"]));
#endif
    throw std::runtime_error("invalid capture type");
}

PipeSpec CaptureFactory::getSpec(JsonValue config) {
    // currently all supported capture types are interleaved
    // so we can use interleavedCapture unconditionally
    // this may change in the future
    ChannelNumber in_channels_count = (ChannelNumber)config["channels"];
    SampleFormat in_format = SampleFormat::fromName((const char*)config["format"]);
    return InterleavedCapture::outputSpec(in_channels_count, in_format);
}
