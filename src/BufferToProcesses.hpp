#pragma once
#include <boost/process.hpp>
#include <boost/asio.hpp>
#include <vector>
#include "RingBuffer.hpp"
#include <iostream>
#include <assert.h>
#include "ProcessExecArgs.hpp"

class BufferToProcesses {
protected:
    using io_service = boost::asio::io_service;
    static constexpr size_t min_write_amount_ = 1 << 16;
    static constexpr size_t max_write_amount_ = 1 << 22;
    io_service &io_;
    RingBuffer<uint8_t> &ringbuffer_;
    BufferResult<uint8_t> buffer_result_ = nullptr;
    struct Process {
        boost::process::group process_group;
        boost::process::async_pipe pipe_sink;
        boost::process::child process;
        Process(io_service &io, const std::string exe_path, const std::vector<std::string> args): pipe_sink(io) {
            process = boost::process::child(exe_path, process_group, boost::process::args(args),
                boost::process::std_in < pipe_sink,
                boost::process::std_out > boost::process::null
            );
        }
        void close() {
            if (pipe_sink.is_open()) {
                pipe_sink.close();
            }
        }
    };
    std::vector<Process> processes_;
    bool closing_ = false;
    size_t writes_remaining_ = 0;
    size_t failed_writes_ = 0;
    bool finish_scheduled_ = false;
    size_t finish_in_bytes_ = 0;
    size_t total_bytes_ = 0;
    void closeIfRequested() {
        if (closing_) {
            for (auto &p: processes_) {
                p.close();
            }
        }
    }
    bool operationPending() const {
        return writes_remaining_!=0;
    }
public:
    BufferToProcesses(io_service &io, RingBuffer<uint8_t> &ringbuffer, const size_t processes_count): io_(io), ringbuffer_(ringbuffer) {
        processes_.reserve(processes_count);
    }
    void addProcess(const std::string exe_path, const std::vector<std::string> args) {
        processes_.emplace_back(io_, exe_path, args);
    }
    void addProcess(const ProcessExecArgs pea) {
        addProcess(pea.exe_path, pea.args);
    }
    bool closed() {
        size_t alive_count = 0;
        for (const Process &p: processes_) {
            if (p.pipe_sink.is_open()) {
                alive_count++;
            }
        }
        if (alive_count==0) {
            return true;
        } else if (alive_count==processes_.size() || closing_) {
            return false;
        } else {
            // half-dead, kill'em all!
            for (Process &p: processes_) {
                if (p.pipe_sink.is_open()) {
                    p.pipe_sink.close();
                }
            }
            return true;
        }
    }
    bool readyToDestroy() {
        for (Process &p: processes_) {
            if (p.pipe_sink.is_open() || p.process.running() ) {
                return false;
            }
        }
        return true;
    }
    size_t totalBytes() const { return total_bytes_; }
    void pollBuffer() {
        //std::cerr << "pollBuffer begin\n";
        if (operationPending()) return;
        
        //std::cerr << "pollBuffer after preliminary tests\n";
        size_t min_bytes = min_write_amount_;
        size_t max_bytes = max_write_amount_;
        if (finish_scheduled_) {
            if (finish_in_bytes_==0) {
                finish();
                closeIfRequested();
                return;
            }
            if (finish_in_bytes_ < min_bytes) min_bytes = finish_in_bytes_;
            if (finish_in_bytes_ < max_bytes) max_bytes = finish_in_bytes_;
        }
        if (closing_) min_bytes = 1;
        buffer_result_ = ringbuffer_.getReadBuffer(min_bytes, max_bytes);
        if (!buffer_result_) {
            closeIfRequested();
            return;
        }
        //std::cerr << "pollBuffer got " << buffer_result_.count() << " bytes\n";
        writes_remaining_ = processes_.size();
        failed_writes_ = 0;
        bool c_finish_scheduled = finish_scheduled_; // we must copy the variable to prevent race condition between async_write call and callback
        auto asio_buffer = boost::asio::buffer((const void*)buffer_result_.begin(), buffer_result_.count());
        for (Process &p: processes_) {
            boost::asio::async_write(p.pipe_sink, asio_buffer, [this, &p, c_finish_scheduled](const boost::system::error_code &ec, size_t nbytes) {
                //std::cerr << "async write callback begin\n";
                writes_remaining_ -= 1;
                bool failed = false;
                if (ec) {
                    std::cerr << "Error writing to pipe: " << ec.message() << std::endl;
                    failed = true;
                } else if (nbytes != buffer_result_.count()) {
                    std::cerr << "Error writing to pipe, only " << nbytes << "B written, should have written " << buffer_result_.count() << "B" << std::endl;
                    failed = true;
                }
                if (failed) {
                    failed_writes_++;
                    p.pipe_sink.close();
                }
                if (writes_remaining_==0) {
                    if (failed_writes_==0) { // prefer writing twice to losing data
                        if (c_finish_scheduled) {
                            assert(nbytes <= finish_in_bytes_);
                            finish_in_bytes_ -= nbytes;
                        }
                        ringbuffer_.readDone(nbytes);
                        total_bytes_ += nbytes;
                    }
                    if (c_finish_scheduled && (finish_in_bytes_ == 0)) {
                        finish();
                        closeIfRequested();
                    } else if (buffer_result_.isMoreAvailable()) {
                        if (!closed()) pollBuffer();
                    } else {
                        closeIfRequested();
                    }
                }
                //std::cerr << "async write callback end\n";
            });
        }
    }
    bool isFinishScheduled() const { return finish_scheduled_; }
    void finish() {
        closing_ = true;
    }
    void finishIn(const size_t bytes) {
        assert(!finish_scheduled_);
        finish_in_bytes_ = bytes;
        finish_scheduled_ = true;
    }
    void finishWhenTotalBytesReach(const size_t bytes) {
        assert(!finish_scheduled_);
        assert((bytes-total_bytes_) < (SIZE_MAX>>1)); // it ensures that size_t won't overflow
        finish_in_bytes_ = bytes - total_bytes_;
        finish_scheduled_ = true;
    }
    ~BufferToProcesses() {
        assert(!operationPending());
    }
};
