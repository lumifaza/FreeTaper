#pragma once
#include <portaudio.h>
#include <pa_linux_alsa.h>
#include <regex>
#include <iostream>
#include <type_traits>

template<typename ProcPtr> class PortAudioIO {
protected:
    PaStream* stream_;
    ProcPtr proc_;
    int sample_rate_ = -1;
    int latency_ = -1;
    static int paCallback(const void *input, void *output, unsigned long frameCount,
                          const PaStreamCallbackTimeInfo* timeInfo, PaStreamCallbackFlags statusFlags,
                          void *userData) {
        ProcPtr proc = static_cast<ProcPtr>(userData);
        if (proc->inputChannelsCount()>0) {
            if (statusFlags & (paInputUnderflow | paInputOverflow)) {
                proc->overflow();
            }
        }
        if (proc->outputChannelsCount()>0) {
            if (statusFlags & (paOutputUnderflow | paOutputOverflow)) {
                proc->overflow();
            }
        }
        if (! (statusFlags & paPrimingOutput) ) {
            proc->processBuffer(frameCount, static_cast<typename std::remove_reference<decltype(*proc)>::type::InputBuffer>(input), static_cast<typename std::remove_reference<decltype(*proc)>::type::OutputBuffer>(output));
        }
        return paContinue;
    }
public:
    PortAudioIO(ProcPtr proc, const std::regex &match_api, const std::regex &match_device, const double preferred_sample_rate = 0, const PaTime preferred_latency = -1):
        proc_(proc) {
        auto paerr = Pa_Initialize();
        if (paerr != paNoError) {
            throw std::runtime_error("Unable to initialize PortAudio: " + std::string(Pa_GetErrorText(paerr)));
        }
        int devcount = Pa_GetDeviceCount();
        int dev_selected = -1;
        const PaDeviceInfo *devinfo = nullptr;
        for (int i=0; i<devcount; i++) {
            devinfo = Pa_GetDeviceInfo(i);
            const PaHostApiInfo *hostinfo;
            hostinfo = Pa_GetHostApiInfo(devinfo->hostApi);
            std::cout << i << ": " << hostinfo->name << " / " << devinfo->name << " (" << devinfo->maxInputChannels << "/" << devinfo->maxOutputChannels << ")" << std::endl;
            if ((dev_selected<0) && std::regex_match(hostinfo->name, match_api) && std::regex_match(devinfo->name, match_device)) {
                std::cout << "Using it." << std::endl;
                dev_selected = i;
            }
        }
        if (dev_selected < 0) {
            throw std::runtime_error("Couldn't find device.");
        }
        PaStreamParameters par_common = {0};
        PaStreamParameters par_in = {0};
        PaStreamParameters par_out = {0};
        PaStreamParameters* ppar_in = nullptr;
        PaStreamParameters* ppar_out = nullptr;
        par_common.device = dev_selected;
        par_common.sampleFormat = proc_->paSampleFormat();
        par_common.hostApiSpecificStreamInfo = NULL;
        if (proc_->inputChannelsCount()>0) {
            par_in = par_common;
            par_in.suggestedLatency = (preferred_latency >= 0) ? preferred_latency : devinfo->defaultHighInputLatency;
            par_in.channelCount = proc_->inputChannelsCount();
            ppar_in = &par_in;
        }
        if (proc_->outputChannelsCount()>0) {
            par_out = par_common;
            par_out.suggestedLatency = (preferred_latency >= 0) ? preferred_latency : devinfo->defaultHighOutputLatency;
            par_out.channelCount = proc_->outputChannelsCount();
            ppar_out = &par_out;
        }
        auto tryOpen = [&](const double srate) -> bool {
            PaError err = Pa_OpenStream(&stream_, ppar_in, ppar_out, srate, 0, paNoFlag, paCallback, proc_);
            if (err == paNoError) {
                if (Pa_GetStreamHostApiType(stream_)==paALSA) {
                    PaAlsa_EnableRealtimeScheduling(stream_, 1);
                }
                const PaStreamInfo* info = Pa_GetStreamInfo(stream_);
                sample_rate_ = info->sampleRate;
                latency_ = info->inputLatency * (double)sample_rate_;
                return true;
            } else {
                std::cerr << "Opening device with sample rate = " << srate << " failed." << std::endl;
                return false;
            }
        };
        if (preferred_sample_rate>0) {
            if (tryOpen(preferred_sample_rate)) return;
        }
        if (tryOpen(devinfo->defaultSampleRate)) return;
        throw std::runtime_error("Opening audio device failed.");
    }
    void start() {
        Pa_StartStream(stream_);
    }
    void stop() {
        Pa_StopStream(stream_);
    }
    int sampleRate() const {
        return sample_rate_;
    }
    int latency() const {
        return latency_;
    }
    ~PortAudioIO() {
        Pa_CloseStream(stream_);
        Pa_Terminate();
    }
};
