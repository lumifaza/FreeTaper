
BUILD_TYPE = Debug

ifeq ($(BUILD_TYPE),Debug)
OPTIMIZATION_FLAGS = -O0 -ftrapv
else
OPTIMIZATION_FLAGS = -O3 -flto
endif

override CXXFLAGS += -g -std=c++11 -Ideps/include $(OPTIMIZATION_FLAGS)
override CFLAGS += -g $(OPTIMIZATION_FLAGS)
override LFLAGS += $(OPTIMIZATION_FLAGS)

CPPSRC = freetaper.cpp util.cpp SampleFormat.cpp SampleConverter.cpp strutils.cpp globals.cpp capture.cpp

BUILD_DATE_FILE = builddate.h
CONFIG_H_FILE = config.h
# $(dir $(realpath $(firstword $(MAKEFILE_LIST))))/
SRCDIR = src
DEPS_LIBS = deps/hjson-cpp/build/src/libhjson.a
LIBS_FLAGS = -lm -lpthread -lboost_system -lboost_iostreams -lboost_filesystem -lportaudio
EXE = freetaper
WRITEWAV_EXE = freetaper-writewav
CPPSRC_ALL = $(patsubst %,$(SRCDIR)/%,$(CPPSRC))

BUILDDIR := objs
DEPFLAGS = -MT $@ -MMD -MP -MF $(DEPDIR)/$*.Td
DEPDIR := $(BUILDDIR)
POSTCOMPILE = @mv -f $(DEPDIR)/$*.Td $(DEPDIR)/$*.d && touch $@

.PHONY: builddate build install clean clean_deps
.DEFAULT_GOAL := build

builddate:
	( /bin/date '+#define COMPILE_DATE "%Y-%m-%d %H:%M:%S %z"' && \
	echo '#define GIT_VERSION "$(shell git describe --abbrev --dirty --always --tags)"' ) > $(BUILD_DATE_FILE)

$(BUILD_DATE_FILE): builddate

$(BUILDDIR)/src/SampleConverter.o: $(BUILDDIR)/get_conversion_function.generated.cpp

$(patsubst %.cpp,$(BUILDDIR)/%.o,$(CPPSRC_ALL)): $(BUILDDIR)/%.o: %.cpp $(CONFIG_H_FILE)
	@mkdir -p $(dir $@)
	$(CXX) $(CXXFLAGS) $(DEPFLAGS) -c -o $@ $< -include $(CONFIG_H_FILE)
	$(POSTCOMPILE)

$(CONFIG_H_FILE):
	test -e $@ || cp config.h.default config.h

$(BUILDDIR)/app_version.o: src/app_version.cpp builddate $(BUILD_DATE_FILE)
	@mkdir -p $(dir $@)
	$(CXX) $(CXXFLAGS) -c -o $@ $< -include $(BUILD_DATE_FILE)

$(BUILDDIR)/get_conversion_function.generated.cpp: $(SRCDIR)/generate_get_conversion_function
	@mkdir -p $(dir $@)
	$< > $@

$(EXE): $(patsubst %.cpp,$(BUILDDIR)/%.o,$(CPPSRC_ALL)) $(BUILDDIR)/app_version.o $(DEPS_LIBS)
	$(CXX) $(CXXFLAGS) $(LFLAGS) -o $@ $^ $(LIBS_FLAGS)

$(WRITEWAV_EXE): $(SRCDIR)/writewav.c
	$(CC) $(CFLAGS) $(LFLAGS) -o $@ $^

build: $(EXE) $(WRITEWAV_EXE)

clean:
	rm $(EXE) $(WRITEWAV_EXE) $(BUILD_DATE_FILE) || true
	rm -r $(BUILDDIR) || true

clean_deps:
	rm -r deps/hjson-cpp/build || true

deps/hjson-cpp/build/src/libhjson.a:
	mkdir -p deps/hjson-cpp/build
	cd deps/hjson-cpp/build && cmake -DCMAKE_BUILD_TYPE=$(BUILD_TYPE) -DCMAKE_CXX_FLAGS="$(CXXFLAGS)" -DCMAKE_EXE_LINKER_FLAGS="$(LFLAGS)" .. && make VERBOSE=1
	# libhjson changes .a name when compiled as Debug
	test -e $@ || ln -s libhjsond.a $@

.PRECIOUS: $(BUILDDIR)/%.d

include $(wildcard $(patsubst %.cpp,$(BUILDDIR)/%.d,$(CPPSRC_ALL)))
